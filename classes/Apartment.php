<?php

require_once 'array.php';
require_once 'classes/ApartmentWriter.php';

class Apartment{
    public $title = '';
    public $type = '';
    public $address = '';
    public $price = '';
    public $description = '';
    public $kittchen = '';


    public function __construct($title, $type, $address, $price, $description, $kittchen){
     $this -> title = $title;  
     $this -> type = $type;  
     $this -> address = $address;  
     $this -> price = $price;  
     $this -> description = $description;  
     $this -> kittchen =  $kittchen;  
    }

      public function getSummaryLine() {
        return '
        <ul>
        <li><strong>Название </strong>' .$this -> title. '</li>
        <li><strong>Тип </strong>' .$this -> type. '</li>
        <li><strong>Адрес </strong>' .$this -> address. '</li>
        <li><strong>Цена за сутки </strong>' .$this -> price . " EUR ". '</li>
        <li><strong>Кухня </strong>' .$this -> kittchen. '</li>
        </ul>
        ';
    }
   
}



?>